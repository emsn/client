# client
```
docker build --pull -t ansi/emsn:latest .
docker push            ansi/emsn:latest
docker run -it --rm --privileged -v /dev/bus/usb:/dev/bus/usb -v /home/pirate/emsn:/host -e TZ="Europe/Berlin" ansi/emsn:latest
```

# transmitter
```
cd transmitter
docker build --pull -t ansi/emsn-transmitter:latest .
docker push            ansi/emsn-transmitter:latest
docker run -it --rm -v /home/pirate/emsn:/host -e TZ="Europe/Berlin" ansi/emsn-transmitter:latest
```
