import os
import csv
import json
import time
import requests
import datetime
from   dateutil import parser

work_dir          = os.getenv('TRANSMITTER_WORKDIR', '/host')
groundstation_id  = os.getenv('GROUNDSTATION_ID',    1)
groundstation_key = os.getenv('GROUNDSTATION_KEY',   '12345678901234567890123')


class Transmitter():

    def __init__(self, workdir, groundstation_id, groundstation_key):
        self.work_dir          = work_dir
        self.groundstation_id  = groundstation_id
        self.groundstation_key = groundstation_key

    def getConfig(self):
        r = requests.get("https://api.emsn.eu/v0/clientconfig/?station_id=%d&api_key=%s" % (self.groundstation_id, self.groundstation_key), timeout = 30)

        if r.status_code == 200:
            self.config = r.json()

            with open('%s/emsn_job_config' % self.work_dir, 'w') as config_file:
                config_file.writelines("LOW=%d\n"         % self.config['frequency_low']    )
                config_file.writelines("HIGH=%d\n"        % self.config['frequency_high']   )
                config_file.writelines("CHAIN=%d\n"       % self.config['frequency_chain']  )
                config_file.writelines("GAIN=%d\n"        % self.config['gain']             )
                config_file.writelines("INTEGRATION=%d\n" % self.config['integration_time'] )
                config_file.writelines("TIME=%d\n"        % self.config['time_to_run']      )

    def getNextFilename(self):
        onlyfiles = [f for f in os.listdir(self.work_dir) if f!="emsn_job_config" and os.path.isfile(os.path.join(self.work_dir, f))]
        onlyfiles.sort()

        for f in onlyfiles:
            ff   = os.path.join(self.work_dir, f)
            stat = os.stat(ff)
            if (time.time() - stat.st_mtime) > 50:
                return f

        return None

    def upstream(self, low, high, chain, record):

        if len(record) > 0:
            tobject = {}
            tobject['groundstation_id']  = self.groundstation_id
            tobject['api_key']           = self.groundstation_key
            tobject['frequency_low']     = low
            tobject['frequency_high']    = high
            tobject['chain']             = chain
            tobject['recording']         = record
            tobject['temperature']       = 23.5
            r = requests.post("https://api.emsn.eu/v0/recording/", data=json.dumps(tobject), headers={"accept": "application/json", "Content-Type": "application/json"}, timeout=60)

            if r.status_code == 200:
                print(r.json())
            else:
                print(r)
                print(r.text)

    def transmit(self):
        filename = self.getNextFilename()

        while filename:
            print ("Transmitting now: %s" % filename)

            with open('%s/%s' % (self.work_dir, filename), 'r') as f:
                reader     = csv.reader(f)
                doublelist = list(reader)
                low        = 0
                high       = 0
                chain      = 0
                frequency  = 0
                db         = 0
                record     = ""

                for row in doublelist:
                    rdate     = parser.parse('%s %s' %(row[0], row[1]))
                    seconds   = int( (rdate - datetime.datetime(2020, 1, 1) ).total_seconds())
                    new_low   = int(row[2])
                    new_high  = int(row[3])
                    new_chain = float(row[4])

                    if low != new_low or high != new_high or chain != new_chain:
                        self.upstream(low, high, chain, record)
                        low = new_low
                        high = new_high
                        chain = new_chain
                        record = ""

                    line = "%s" % seconds

                    for e in row[6:]:
                        line += ",%s" % e

                    record += "%s\n" % line

                self.upstream(low, high, chain, record)

            os.remove('%s/%s' % (self.work_dir, filename) )
            filename = self.getNextFilename()

if __name__ == '__main__':
    print("Transmitter started")
    print("Workin on: %s" % work_dir)
    transmitter = Transmitter(work_dir, groundstation_id, groundstation_key)

    while True:
        transmitter.getConfig()
        transmitter.transmit()
        print("sleeping")
        time.sleep(120)
