#!/bin/bash

cd /host
. emsn_job_config

#avoid core dump
ulimit -S -c 0

LINES=`/bin/echo "500000 / ((${HIGH}-${LOW}) / (${INTEGRATION} * ${CHAIN}))" | /usr/bin/bc`

/usr/local/bin/rx_power -f ${LOW}:${HIGH}:${CHAIN}                \
                        -e ${TIME}                                \
                        -i ${INTEGRATION}                         \
                        -g ${GAIN}                                \
                        -                                         \
                        | /usr/bin/split                          \
                        --suffix-length 5                         \
                        --additional-suffix _emsn_`/bin/date +%s` \
                        --lines ${LINES}
