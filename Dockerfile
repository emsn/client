FROM resin/rpi-raspbian:stretch

# System
RUN apt update && apt upgrade -y
RUN apt install -y gnuradio build-essential git libusb-dev rtl-sdr unzip wget bc \
                   usbutils cmake g++ libpython-dev python-numpy swig less mc vim

# SDRPlay
ADD package/mirsdrapi-rsp.h /usr/local/include/mirsdrapi-rsp.h
RUN chmod 644               /usr/local/include/mirsdrapi-rsp.h
ADD package/libmirsdrapi-rsp.so.2.13 /usr/local/lib/libmirsdrapi-rsp.so.2.13
RUN chmod 644                        /usr/local/lib/libmirsdrapi-rsp.so.2.13
RUN ln -s                            /usr/local/lib/libmirsdrapi-rsp.so.2.13 /usr/local/lib/libmirsdrapi-rsp.so.2
RUN ln -s                            /usr/local/lib/libmirsdrapi-rsp.so.2 /usr/local/lib/libmirsdrapi-rsp.so
ADD package/66-mirics.rules /etc/udev/rules.d/66-mirics.rules
RUN chmod 644               /etc/udev/rules.d/66-mirics.rules
RUN ldconfig

# Soapy
RUN     wget https://github.com/pothosware/SoapySDR/archive/master.zip
RUN     unzip master.zip && rm master.zip
RUN     mkdir /SoapySDR-master/build
WORKDIR /SoapySDR-master/build
RUN     cmake ..
RUN     make -j 5
RUN     make install
RUN     ldconfig
RUN     rm -rf /SoapySDR-master
WORKDIR /

# SoapySDRPlay
RUN     wget https://github.com/pothosware/SoapySDRPlay/archive/master.zip
RUN     unzip master.zip && rm master.zip
RUN     mkdir /SoapySDRPlay-master/build
WORKDIR /SoapySDRPlay-master/build
RUN     cmake ..
RUN     make -j 5
RUN     make install
RUN     ldconfig
RUN     rm -rf /SoapySDRPlay-master
WORKDIR /

# rx_tools
RUN     wget https://github.com/rxseger/rx_tools/archive/master.zip
RUN     unzip master.zip && rm master.zip
WORKDIR /rx_tools-master
RUN     cmake .
RUN     make -j 5
RUN     make install
RUN     rm -rf /rx_tools-master
WORKDIR /

# emsn
ADD     execute.sh /execute.sh
RUN     chmod 777 /execute.sh
VOLUME  /host
CMD     /execute.sh
